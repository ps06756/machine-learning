## Copyright (C) 2015 pratik
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author: pratik <pratik@ps06756>
## Created: 2015-11-01


function val = getInput1 (fname)
x = load(fname);
m = size(x);
val = [ones(m(1),1), x];
endfunction

function val = getInput2(fname)
val = load(fname);
endfunction

% x is a matrix with ones appended as 1st column.
function J = calcError(x, y, theta)
m = size(x);
temp = getMatrix(x,y,theta);
err = temp .^ 2;
J = sum(err) ./ 2*m;
end;

% x and y are given as column vector and theta is also given as column vector.
function val = getMatrix (x, y, theta)
x2(:,1) = x(:,2) * theta(2);
x2(:,1) = x2(:,1) + theta(1);
val = x2(:,1) - y(:,1);
endfunction

function val = scaleDown (x,y,theta, al)
m = size(x);
m1 = getMatrix(x,y,theta);
t1 = theta(1) - (al/m(1))*sum(m1);
xrow(1,:) = x(:,2);
m2 = xrow*getMatrix(x,y,theta);
t2 = theta(2) - (al/m(1))*sum(m2);
theta(1) = t1;
theta(2) = t2;
val = theta;
endfunction

function val = getTheta(fname1, fname2, theta, alpha, iter)
x = getInput1(fname1);
y = getInput2(fname2);
i = 0;
thetaOld = theta;
do
  theta = thetaOld;
  thetaOld = scaleDown(x,y,theta,alpha);
  i++;
until (i  == iter)
val = thetaOld;
figure
plot(x(:,2),y, 'go');
xlabel('Age(y)');
ylabel('Height(m)');
hold on
plot(x(:,2), x*theta, '-');
legend('Training data', 'Linear Regression');
endfunction
